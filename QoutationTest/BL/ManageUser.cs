﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QoutationTest.Common;
using System.Configuration;
using MongoDB.Driver;
using MongoDB.Bson;

namespace QoutationTest.BL
{
    public class ManageUser: mongoHelper, IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public  mbReturnCode GetUserList(out List<UserInfo> ListUserInfo)
        {
            var retCode = mbReturnCode.EXCEPTION;
            ListUserInfo = new List<UserInfo>();
            try
            {
                // LOGIC FOR DATAT RETRIVEL
                // connection string
                string conn = ConfigurationManager.AppSettings["DB_ConnectionString"];
                // create server
                var client = new MongoClient(conn);
                // query on server
                var arrUser = mongoClient.GetDatabase("punjani_live").GetCollection<UserInfo>("login_details").AsQueryable();
                // get data from query
                ListUserInfo = arrUser.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
            }
            return retCode;

        }
        public mbReturnCode DeleteUser(UserInfo arrUserInfo)
        {
            var retCode = mbReturnCode.EXCEPTION;
            //ObjectId objID = ObjectId.Parse(id);
            try
            {
                string conn = ConfigurationManager.AppSettings["DB_ConnectionString"];
                var client = new MongoClient(conn);

                var deleteFilter = Builders<UserInfo>.Filter.Eq("id", arrUserInfo.id
                    //MongoDB.Bson.ObjectId.Parse(id)
                    );
                
                var del = mongoClient.GetDatabase("punjani_live").GetCollection<UserInfo>("login_details").DeleteMany(deleteFilter);
                if (del.DeletedCount != 0)
                {
                    retCode = mbReturnCode.SUCCESS;
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
               
            }
            return retCode;
        }
    }
}
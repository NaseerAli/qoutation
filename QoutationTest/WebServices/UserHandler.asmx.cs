﻿using QoutationTest.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using QoutationTest.org;
namespace QoutationTest.WebServices
{
    /// <summary>
    /// Summary description for UserHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class UserHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer objUserData = new JavaScriptSerializer();
        [WebMethod(EnableSession =true)]
        public string GetAllUserData()
        {
            try
            {
                var objClass = new BL.ManageUser();
                var ListUser = new List<Common.UserInfo>();
                var retCode = objClass.GetUserList(out ListUser);
                return objUserData.Serialize(new { retCode=1,ListUser });
            }
            catch (Exception ex)
            {
                return objUserData.Serialize(new { retCode = 0, Message =ex.Message }); 
            }
        }


        [WebMethod(EnableSession = true)]
        public string DeleteSelectedUser(UserInfo arrUser)
        {
            try
            {
                var objClass = new BL.ManageUser();
                var retCode = objClass.DeleteUser(arrUser);
                return objUserData.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                return objUserData.Serialize(new { retCode = 0, Message = ex.Message });
            }
        }
    }
}

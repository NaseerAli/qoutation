﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Data;
using QoutationTest.BL;
using QoutationTest.Common;

namespace QoutationTest.WebServices
{
    /// <summary>
    /// Summary description for GeneralHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class GeneralHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer objSerializer = new JavaScriptSerializer();
        //[WebMethod(EnableSession = true)]
        //public void LoginUser(string UserName, string Password)
        //{
        //    try
        //    {
        //        var arrData = classes.GeneralManager.UserLogin(UserName, Password);
        //        DTResult.setResponse(new { retCode = 1, arrUser = arrData });
        //    }
        //    catch (Exception ex)
        //    {

        //        DTResult.setResponse(new { retCode = 0, Message = ex.Message });
        //    }
        //}
        //[WebMethod(EnableSession =  true)]
        //public string LoginUser(string UserName, string Password)
        //{
        //    try
        //    {
        //        string json = "";
        //        return json = GeneralManager.UserLogin(UserName, Password);

        //    }
        //    catch (Exception ex)
        //    {

        //        throw new Exception(ex.Message);
        //    }
        //}

        //[WebMethod(EnableSession = true)]
        //public void Registeruser(string FullName, string Email, string Mobile, string Country, string State, string City, string Address, string Zipcode, string Password)
        //{
        //    try
        //    {
        //        var retCode = classes.GeneralManager.Registeruser(FullName, Email, Mobile, Country, State, City, Address, Zipcode, Password);
        //        if (retCode == classes.mongoHelper.mbReturnCode.SUCCESS)
        //            DTResult.setResponse(new { retCode = 1 });
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error On Registration.");
        //    }
        //}

        [WebMethod(EnableSession = true)]
        public string RegisterUser(UserInfo arrUser)
        {
            try
            {
                var objUser = new GeneralManager();
                var retCode= objUser.Registeruser(arrUser);
                return objSerializer.Serialize(new { retCode = retCode });

            }catch (Exception ex)
            {
                return objSerializer.Serialize(new { retCode = 0 ,errorMessage= ex.Message });
            }
            finally { }
           
        }
    }
}

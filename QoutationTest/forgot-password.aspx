﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QoutationMaster.Master" AutoEventWireup="true" CodeBehind="forgot-password.aspx.cs" Inherits="QoutationTest.forget_password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">You forgot your password? Here you can easily retrieve a new password.</p>

      <form>
        <div class="input-group mb-3">
          <input type="email" class="form-control" id="txt_frgtEmail" placeholder="Email"/>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="button" onclick="ForgetPass()" class="btn btn-primary btn-block">Request new password</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mt-3 mb-1">
        <a href="index.aspx">Login</a>
      </p>
      <p class="mb-0">
        <a href="register.aspx" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>


</asp:Content>

﻿function post(url,data,success,error) {
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d);
            if (result.retCode == 1) {
                success(result);
            } else if (result.retCode == 0) {
                error(result);
                console.log(result.errorMessage)
            }
        },
    });
}
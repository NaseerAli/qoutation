﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserAdminData.Master" AutoEventWireup="true" CodeBehind="UsersDetails.aspx.cs" Inherits="QoutationTest.UsersDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="assets/plugins/jquery/jquery.min.js"></script>
<script src="scripts/UserDetails.js?v=1.3"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="card-body p-0 row">
        <table class="table table-condensed" id="tbl_Users">
            <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Full Name</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Password</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        
     </div>

</asp:Content>
